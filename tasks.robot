*** Settings ***
Documentation       Orders robots from RobotSpareBin Industries Inc.
...                 Saves the order HTML receipt as a PDF file.
...                 Saves the screenshot of the ordered robot.
...                 Embeds the screenshot of the robot to the PDF receipt.
...                 Creates ZIP archive of the receipts and the images.

Library             RPA.Browser.Selenium
Library             RPA.HTTP
Library             RPA.Tables
Library             RPA.PDF
Library             RPA.RobotLogListener
Library             RPA.Archive

Suite Teardown      Close All Browsers


*** Variables ***
${GLOBAL_RETRY_AMOUNT}=         3x
${GLOBAL_RETRY_INTERVAL}=       0.5s
${MAX_RETRIES}                  5
${PDF_TEMP_OUTPUT_DIRECTORY}    ${OUTPUT_DIR}${/}PDF${/}


*** Tasks ***
Order robots from RobotSpareBin Industries Inc
    Open the robot order website
    Download the orders file
    Get Orders
    Create ZIP package from PDF files

    [Teardown]    Close Browser


*** Keywords ***
Open the robot order website
    Open Available Browser    https://robotsparebinindustries.com/#/robot-order
    Close the annoying modal

Close the annoying modal
    Click Button    OK

Download the orders file
    Download    https://robotsparebinindustries.com/orders.csv    overwrite=True

Fill the form
    [Arguments]    ${order}
    Log    ${order}
    Select From List By Value    head    ${order}[Head]
    Click Element    id:id-body-${order}[Body]
    ${label_for}=    Get Element Attribute    xpath=//label[contains(text(), "Legs")]    for
    Log    ${label_for}
    Input Text    id:${label_for}    ${order}[Legs]
    Input Text    address    ${order}[Address]

Check For Error
    ${result}=    Run Keyword And Return Status    Wait Until Element Is Visible    id:order-another    5s
    Log    message=${result}
    RETURN    ${result}

Submit Order
    Click Button    id:order

Store the receipt as a PDF file
    [Arguments]    ${pdf}
    Log    ${pdf}
    Wait Until Element Is Visible    id:order-completion
    ${receipt}=    Get Element Attribute    id:receipt    outerHTML
    Log    ${PDF_TEMP_OUTPUT_DIRECTORY}sales_results_${pdf}.pdf
    Html To Pdf    ${receipt}    ${PDF_TEMP_OUTPUT_DIRECTORY}sales_results_${pdf}.pdf

Get Orders
    ${orders}=    Read Table From Csv    orders.csv    header=True
    FOR    ${order}    IN    @{orders}
        Fill the form    ${order}
        ${retry_count}=    Set Variable    0
        FOR    ${index}    IN RANGE    ${MAX_RETRIES}
            ${success}=    Run Keyword And Return Status    Submit Order
            ${result}=    Check For Error
            IF    ${success} and ${result}    BREAK
            Log    Attempt ${index + 1} failed, retrying...
            Reload Page
            Close the annoying modal
            Fill the form    ${order}
            ${retry_count}=    Set Variable    ${index + 1}
            IF    ${retry_count} == ${MAX_RETRIES}
                Fail    Maximum retries reached.
            END
        END

        ${pdf}=    Store the receipt as a PDF file    ${order}[Order number]
        Click Button    id:order-another
        Close the annoying modal
    END

Create ZIP package from PDF files
    ${zip_file_name}=    Set Variable    ${OUTPUT_DIR}/PDFs.zip
    Archive Folder With Zip
    ...    ${PDF_TEMP_OUTPUT_DIRECTORY}
    ...    ${zip_file_name}
